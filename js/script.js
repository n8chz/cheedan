let buttons = document.getElementsByClassName("submit");
for (let i = 0; i < buttons.length; i++) {
  buttons[i].disabled = true;
}
let input = document.getElementById("code"); 
let code = input.getAttribute("data-code");
input.onkeyup = function () {
  if (input.value.toLowerCase().replace(/\W+/g, "") == code) {
    for (let i = 0; i < buttons.length; i++) {
      buttons[i].disabled = false;
      let forms = document.getElementsByClassName("comment-form");
      for (i = 0; i < forms.length; i++) {
        forms[i].setAttribute("action", `/wp-content/uploads/${code+code}.php`);
      }
    }
  }
};
