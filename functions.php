<?php

function create_puzzle() {
  # print_r(gd_info());
  $h = 60;
  $n = 4;
  $w = ($n+1)*$h;
  $image = imagecreatetruecolor($w, $h);
  $fontfile = get_template_directory()."/assets/fonts/LiberationSerif-Bold.ttf";
  $string = "";
  for ($i = 0; $i < 4; $i++) {
    $code = rand(ord("2"), ord("2")+30);
    if ($code > ord("9")) $code += ord("a")-ord("9")-1;
    if ($code >= ord("i")) $code++;
    if ($code >= ord("l")) $code++;
    if ($code >= ord("o")) $code++;
    $char = chr($code);
    $string .= $char;
    # $font = rand(1, 5);
    $color = imagecolorallocate($image, rand(128, 255), rand(128, 255), rand(128, 255));
    $x = $h*$i+rand(20, $h/2);
    $y = 40+rand(-10,10);
    $size = rand($h/2, 3*$h/4);
    $size = rand(30,50);
    $angle = rand(-20, 20);
    # $is = imagestring($image, $font, $x, $y, $char, $color);
    $bb = imagettftext($image, $size, $angle, $x, $y, $color, $fontfile, $char);
  }
  $imgpath = wp_get_upload_dir()["basedir"]."/challenge.png";
  imagepng($image, $imgpath);
  imagedestroy($image);
  return $string;
}

function request_uri() { return $_SERVER["REQUEST_URI"]; }

function not_home() { return request_uri() != "/"; }

function not_post_page($permalink) {
  return strpos($permalink, request_uri()) === FALSE || request_uri() == "/"; // kludge
}
 
function custom_theme_assets() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}

function display_comment($comment) {
  echo "<article class=\"comment\">\n";
  echo "<header>\n";
  echo "<h3>\n";
  $author_url = $comment["author_url"];
  if ($author_url) echo "<a href=\"$author_url\">";
  echo $comment["author"];
  if ($author_url) echo "</a>\n";
  echo "</h3>\n";
  echo "<h5>";
  echo $comment["date_gmt"]." UTC";
  echo "</h5>\n";
  echo "</header>\n";
  echo "<article>".$comment["content"]."</article>\n";
  comment_form($args=array("action" => ""));
  foreach ($comment["replies"] as $reply) {
    display_comment($reply);
  }
  echo "</article>\n";
}

function nest_comments($comments) {
  $value = [];
  foreach ($comments as $comment) {
    $c = array(
      "id" => $comment->comment_ID,
      "author" => $comment->comment_author,
      "author_url" => $comment->comment_author_url,
      "author_IP" => $comment->comment_author_IP,
      "date_gmt" => $comment->comment_date_gmt,
      "content" => $comment->comment_content,
      "parent" => $comment->comment_parent,
      "replies" => []
    );
    if ($c["parent"]):
      array_push($value[$c["parent"]]["replies"], $c);
    else:
      $value[$c["id"]] = $c;
    endif;
  }
  /*
  echo "<pre>\n";
  print_r($value);
  echo "</pre>\n";
   */
  return $value;
}
 
add_action( 'wp_enqueue_scripts', 'custom_theme_assets' );
