<!DOCTYPE html>
<html <?php language_attributes(); ?>>
 
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title>
<?php

if (is_single()) {
  the_title();
  echo " &mdash; ";
}
bloginfo("name");

?>
    </title>
    <?php wp_head() ?>
</head>
 
<body <?php body_class(); ?>>
 
<header class="site-header">
  <h1>
  <?php
    wp_reset_query();
    if (!is_home()) echo "<a href=\"".home_url()."\">";
    echo get_bloginfo("name");
    if (!is_home()) echo "</a>";
    echo "\n";
  ?>
  </h1>
  <h3><?php bloginfo( 'description' ); ?></h3>
</header>
