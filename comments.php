<?php



// cribbed from
// https://developer.wordpress.org/themes/template-files-section/partial-and-miscellaneous-template-files/comment-template/
// and modified

//Get only the approved comments
$args = array(
  'status' => 'approve',
  'post_id' => get_the_ID(),
  'hierarchical' => TRUE
);
 
// The comment Query
$comments_query = new WP_Comment_Query;
$comments = $comments_query->query( $args );
 
// Comment Loop
if ( $comments ) {
  $nest = nest_comments($comments);
  $n = count($nest);
  foreach ($nest as $comment) {
    display_comment($comment);
  }
} else {

  comment_form();
}
