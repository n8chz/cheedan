<?php
  ini_set('display_errors', '1');
  ini_set('display_startup_errors', '1');
  error_reporting(E_ALL);
   
  get_header();
  // echo "<div id=\"main\">\n";
  if (have_posts()):
    while (have_posts()):
      the_post();
      get_template_part("content");
    endwhile;
  else:
    echo '<p>There are no posts!</p>';
  endif;
  the_posts_pagination();
?>

<div id="primary" class="widget-area">
  <?php
    get_search_form();
    echo "<br />\n";
    wp_loginout();
    echo "<br />\n";
    echo "<a href=\"";
    bloginfo("rss_url");
    echo "\">posts rss</a>\n";
  ?>
</div>

<?php
  get_footer();
?>
