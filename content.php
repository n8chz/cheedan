<?php

echo "<article class=\"post\">\n";
echo "<header>\n";
echo "<h2 class=\"entry-title\">";
$permalink = get_permalink();
if (!is_singular()) echo "<a href=\"$permalink\">";
echo get_the_title();
if (!is_singular()) echo "</a>";
echo "</h2>\n";
echo "<h4>".get_the_author()." &mdash; ".get_the_date("Y-m-d h:m:s")."</h4>\n";
echo "</header>\n";
echo "<article class=\"entry-content\">";
the_content();
echo "</article>\n";
echo "<footer class=\"comment comment-footer\">\n";
$n = get_comments_number();
if (is_single()):
  wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true);
  $code = create_puzzle();
  $upload_dir = wp_get_upload_dir()["basedir"];
  $upload_url = wp_get_upload_dir()["baseurl"];
  $template_directory = get_template_directory();
  # TODO: delete previous action script copy
  # wp-comments-post.php assumes it's in top wp directory
  # Must also account for that.
  $c = copy($template_directory."/wp-comments-post.php", $upload_dir."/".$code.$code.".php");
  # echo "<h1>".($c ? "yes" : "no")."</h1>\n";
  echo "<img src=\"".$upload_url."/challenge.png\" />\n";
  echo "<br />\n";
  echo "<label>characters seen above<input id=\"code\" data-code=\"".$code."\" /></label>\n";
  comments_template();
else:
  $comment_msg = $n ? "$n comment(s)" : "leave a comment";
  echo "<a href=\"$permalink\">$comment_msg</a>\n";
endif;
echo " </footer>\n";
echo "</article>\n";

?>

